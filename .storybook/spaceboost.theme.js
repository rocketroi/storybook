import { create } from "@storybook/theming";

const brandUrl = "https://www.spaceboost.com";
const brandTitle = "Storybook Spaceboost";

export default create({
  base: "light",
  brandTitle,
  brandUrl,
  appBorderRadius: 20
});
