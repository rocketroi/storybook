const path = require('path');

module.exports = ({ config }) => {
  config.module.rules.push({
    test: /\.vue$/,
    loader: 'storybook-addon-vue-info/loader',
    options: {
      docgenOptions: {
        alias: config.resolve.alias,
      }
    },
    enforce: 'post',
  });

  config.module.rules.push(
    {
      test: [/\.scss$/, /\.sass$/],
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        { loader: 'sass-loader',
          options: {
            data: '@import "../src/assets/styles/main.sass";',
            includePaths:[__dirname, 'src'],
            sourceMap: true,
            indentedSyntax: true,
          }
        },
        { loader: 'sass-resources-loader',
          options: {
            sourceMap: true,
            resources: [
              path.resolve(__dirname, '../src/assets/styles/variables.sass'),
              path.resolve(__dirname, '../src/assets/styles/mixins.sass'),
            ],
          }
        }
      ]
    }
  );

  config.module.rules.push({
    test: /\.(ico|jpe?g|png|gif|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
    loader: 'file-loader',
    options: {
      name: '[path][name].[ext]',
    },
  });

  config.resolve.alias['@'] = path.join(__dirname, '..', 'src/');

  return config;
};
