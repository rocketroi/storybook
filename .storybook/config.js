import { addParameters, configure, addDecorator } from '@storybook/vue';
import { withInfo } from 'storybook-addon-vue-info';
import centered from '@storybook/addon-centered/vue';

import SpaceboostTheme from './spaceboost.theme.js';

addParameters({
  options: {
    theme: SpaceboostTheme
  }
});

addDecorator(withInfo);
addDecorator(centered);

const req = require.context('../stories', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(req);
}

configure(loadStories, module);
