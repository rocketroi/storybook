const path = require('path');

module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    'airbnb-base',
  ],
  plugins: ['vue'],
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            extensions: ['.vue', '.js', '.json'],
            alias: {
              '@': path.resolve(__dirname, 'src'),
            }
          },
        },
      },
    },
  },
  rules: {
    'no-shadow': 0,
    'no-param-reassign': 0,
    'no-plusplus': 0,
    'vue/no-v-html': 'off',
    'max-len': 0,
    'vue/valid-v-on': 0,
    'vue/require-prop-types': 0,
    'vue/require-v-for-key': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never',
    }],
    'import/no-extraneous-dependencies': ['error', {
      optionalDependencies: ['test/unit/index.js']
    }],
  },
};
