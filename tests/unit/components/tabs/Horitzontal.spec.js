import Component from '@/components/tabs/Horitzontal';
import { getComponent } from '../../utils';

describe('Tabs.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.tab')).toBeTruthy();
  });
});
