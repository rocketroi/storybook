import Component from '@/components/notification/Advise';
import { getComponent } from '../../utils';

describe('Advise.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      visible: true,
    });
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.card-notify')).toBeTruthy();
  });
});
