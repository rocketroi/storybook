import Component from '@/components/payment/Card';
import { getComponent } from '../../utils';

describe('Card.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      brand: 'Visa',
      name: 'John Smith',
      number: '1234',
      expiryDate: '12/22',
    });
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.payment-card')).toBeTruthy();
  });

  it('has the correct brand', () => {
    expect(
      component.$el.querySelector('.payment-card__brand').textContent.trim(),
    ).toEqual('Visa');
  });

  it('has the correct name', () => {
    expect(
      component.$el.querySelector('.payment-card__name').textContent.trim(),
    ).toEqual('John Smith');
  });

  it('has the correct number', () => {
    expect(
      component.$el
        .querySelector('.payment-card__number_digits')
        .textContent.trim(),
    ).toEqual('1234');
  });
});
