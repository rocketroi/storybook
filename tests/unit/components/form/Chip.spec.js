import Component from '@/components/form/Chip';
import { getComponent } from '../../utils';

describe('Chip.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.chip')).toBeTruthy();
  });
});
