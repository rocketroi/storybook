import Component from '@/components/form/Switch';
import { getComponent } from '../../utils';

import Vue from 'vue';
import ToggleButton from 'vue-js-toggle-button';

Vue.use(ToggleButton);

describe('FormSwitch.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, { color: 'indigo', value: true });
  });

  // Component
  it('renders correct switch', () => {
    expect(component.$el.querySelector('.vue-js-switch')).toBeTruthy();
  });

  it('initialize with the correct state', () => {
    component = getComponent(Component, { value: false });

    expect(component.$el.querySelector('.vue-js-switch')._prevClass.indexOf('is-checked') !== -1).toBeFalsy();

    component = getComponent(Component, { value: true });

    expect(component.$el.querySelector('.vue-js-switch')._prevClass.indexOf('is-checked') !== -1).toBeTruthy();
  });

  it('renders correct color', () => {
    expect(component.$el.querySelector('.vue-js-switch')._prevClass.indexOf('is-indigo') !== -1).toBeTruthy();
  });

  it('change state', () => {
    expect(component.$el.querySelector('.vue-js-switch')._prevClass.indexOf('is-checked') !== -1).toBeTruthy();
  });

  it('sets disabled', () => {
    expect(component.$el.querySelector('.vue-js-switch')._prevClass.indexOf('disabled') === -1).toBeTruthy();
  });

  it('shows label correctly', () => {
    expect(component.$el.querySelector('span')).toBeNull();

    component = getComponent(Component, { labels: true });

    expect(component.$el.querySelector('span')).toBeTruthy();
  });
});
