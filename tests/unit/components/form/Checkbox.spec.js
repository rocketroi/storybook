import Component from '@/components/form/Checkbox';
import { getComponent } from '../../utils';

describe('FormCheckbox.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.checkbox-container')).toBeTruthy();
  });

  it('add correct disabled state', () => {
    component = getComponent(Component, {
      disabled: true,
    });
    expect(component.$el.querySelector('.checkbox-disabled')).toBeTruthy();
  });

  // Toggle
  it('handles toggle', () => {
    component = getComponent(Component, {
      checked: false,
    });
    expect(component.$el.querySelector('.checkbox-active')).toBeFalsy();
    component = getComponent(Component, {
      checked: true,
    });
    expect(component.$el.querySelector('.checkbox-active')).toBeTruthy();
  });

  // Classes
  it('returns correct default computedClasses', () => {
    expect(component.computedClasses).toEqual({
      'checkbox-disabled': false,
      'checkbox-active': false,
    });
  });

  it('returns correct classes if is disabled', () => {
    const component = getComponent(Component, {
      disabled: true,
    });
    expect(component.computedClasses).toEqual({
      'checkbox-disabled': true,
      'checkbox-active': false,
    });
  });
});
