import Vue from 'vue';
import Component from '@/components/form/Input';
import { getComponent } from '../../utils';

describe('FormInput.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {});
  });

  // Component
  it('renders correct default input type', () => {
    expect(component.$el.querySelector('input').type).toEqual('text');
  });

  it('renders correct custom input type', () => {
    const component = getComponent(Component, { type: 'number' });
    expect(component.$el.querySelector('input').type).toEqual('number');
  });

  // Values
  it('copies the value when provided', () => {
    const component = getComponent(Component, { value: 'some' });
    expect(component.value).toEqual('some');
    expect(component.value).toEqual(component.valueCopy);
  });

  it('watches for the value', (done) => {
    const component = getComponent(Component, { value: 'some' });
    const other = 'other';
    component.value = other;

    Vue.nextTick(() => {
      expect(component.valueCopy).toEqual(other);

      done();
    });
  });

  // Classes
  it('returns correct default computedClasses', () => {
    expect(component.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': false,
      'material--raised': false,
    });
  });

  it('returns correct computedClasses with placeholder', () => {
    const placeholder = getComponent(Component, {
      placeholder: 'placeholder',
    });

    expect(placeholder.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': false,
      'material--raised': true,
    });
  });

  it('returns correct computedClasses with errors', () => {
    const errors = getComponent(Component, {
      errorMessages: ['Error'],
    });

    expect(errors.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': true,
      'material--raised': false,
    });
  });

  // Focus
  it('handles focus', () => {
    expect(component.focus).toEqual(false);

    component.handleFocus(true);
    expect(component.focus).toEqual(true);
  });

  // Validation
  it('sets valid state on valid input', () => {
    const component = getComponent(Component, {
      required: false,
    });

    const input = component.$el.querySelector('input');
    expect(input.validity.valid).toEqual(true);
  });

  it('sets invalid state on invalid input', () => {
    const component = getComponent(Component, {
      required: true,
      type: 'email',
    });

    const input = component.$el.querySelector('input');
    expect(input.validity.valid).toEqual(false);
  });

  it('returns correct computedErrors', () => {
    // Array:
    const errors = ['Error'];
    const listErrors = getComponent(Component, {
      errorMessages: errors,
    });

    expect(listErrors.computedErrors).toEqual(errors);

    // String:
    const error = 'Some error';
    const strErrors = getComponent(Component, {
      errorMessages: error,
    });

    expect(strErrors.computedErrors).toEqual([error]);
  });
});
