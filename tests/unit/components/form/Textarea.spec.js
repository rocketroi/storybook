import Component from '@/components/form/Textarea';
import { getComponent } from '../../utils';

describe('Textarea.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {});
  });

  // Component
  it('renders correct default textarea', () => {
    expect(component.$el.querySelector('textarea')).toBeTruthy();
  });

  it('renders correct label', () => {
    const component = getComponent(Component, { label: 'label0' });
    expect(component.$el.querySelector('label').innerHTML).toEqual('label0');
  });

  it('textarea is readOnly', () => {
    const component = getComponent(Component, { readonly: true });
    expect(component.$el.querySelector('textarea').readOnly).toBeTruthy();
  });

  it('textarea is NOT readOnly', () => {
    const component = getComponent(Component, { readonly: false });
    expect(component.$el.querySelector('textarea').readOnly).toBeFalsy();
  });

  it('textarea is disabled', () => {
    const component = getComponent(Component, { disabled: true });
    expect(component.$el.querySelector('textarea').disabled).toBeTruthy();
  });

  it('textarea is enabled', () => {
    const component = getComponent(Component, { disabled: false });
    expect(component.$el.querySelector('textarea').disabled).toBeFalsy();
  });

  it('returns correct computedClasses with placeholder', () => {
    const placeholder = getComponent(Component, {
      placeholder: 'placeholder',
    });

    expect(placeholder.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': false,
      'material--raised': true,
      'material-input': true,
    });
  });

  it('returns correct computedClasses with errors', () => {
    const errors = getComponent(Component, {
      errorMessages: ['Error'],
    });

    expect(errors.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': true,
      'material--raised': false,
      'material-input': true,
    });
  });

  // Focus
  it('handles focus', () => {
    expect(component.focus).toEqual(false);

    component.handleFocus(true);
    expect(component.focus).toEqual(true);
  });

  // Validation
  it('sets valid state on valid input', () => {
    const component = getComponent(Component, {
      required: false,
    });

    expect(component.$el.querySelector('textarea').validity.valid).toBeTruthy();
  });

  it('sets invalid state on invalid input and class error', () => {
    const component = getComponent(Component, {
      required: true,
    });

    expect(component.$el.querySelector('textarea').validity.valid).toBeFalsy();
  });

  it('returns correct computedErrors', () => {
    const errors = ['Error'];
    const error = 'Some error';
    const listErrors = getComponent(Component, {
      errorMessages: errors,
    });

    expect(listErrors.computedErrors).toEqual(errors);

    const strErrors = getComponent(Component, {
      errorMessages: error,
    });

    expect(strErrors.computedErrors).toEqual([error]);
  });
});
