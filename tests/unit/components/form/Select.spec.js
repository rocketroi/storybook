import Component from '@/components/form/Select';
import { getComponent } from '../../utils';

describe('Select.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.v-select')).toBeTruthy();
  });

  it('renders correct default select type', () => {
    expect(component.$el.querySelector('input').type).toEqual('search');
  });

  // Classes
  it('returns correct default computedClasses', () => {
    expect(component.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': false,
      'material--raised': false,
    });
  });

  it('returns correct computedClasses with errors', () => {
    const errors = getComponent(Component, {
      errorMessages: ['Error'],
    });

    expect(errors.computedClasses).toEqual({
      'material--active': false,
      'material--disabled': false,
      'material--has-errors': true,
      'material--raised': false,
    });
  });

  it('returns correct computedErrors', () => {
    // Array:
    const errors = ['Error'];
    const listErrors = getComponent(Component, {
      errorMessages: errors,
    });

    expect(listErrors.computedErrors).toEqual(errors);

    // String:
    const error = 'Some error';
    const strErrors = getComponent(Component, {
      errorMessages: error,
    });

    expect(strErrors.computedErrors).toEqual([error]);
  });

  // Focus
  it('handles focus', () => {
    expect(component.focus).toEqual(false);

    component.handleFocus(true);
    expect(component.focus).toEqual(true);
  });
});
