import Component from '@/components/avatar/User';
import { getComponent } from '../../utils';

describe('AvatarUser.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  it('renders correct component', () => {
    expect(component.$el.querySelector('.avatar')).toBeTruthy();
  });

  it('sets correct initials', () => {
    const component = getComponent(Component, {
      name: 'Peter',
    });
    expect(component.$el.querySelector('.avatar').dataset.initial).toBe('Pe');
  });

  it('renders correct color', () => {
    const component = getComponent(Component, {
      color: 'indigo',
    });
    expect(component.$el.querySelector('.avatar')._prevClass.indexOf('bg-indigo') !== -1).toBeTruthy();
  });
});
