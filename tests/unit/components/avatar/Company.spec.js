import Component from '@/components/avatar/Company';
import { getComponent } from '../../utils';

describe('AvatarCompany.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      label: 'label',
      type: 'email',
    });
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.card-company')).toBeTruthy();
  });
});
