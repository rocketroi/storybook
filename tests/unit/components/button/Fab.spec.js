import Component from '@/components/button/Fab';
import { getComponent } from '../../utils';

import Vue from 'vue';
import VueFeather from 'vue-feather';

Vue.use(VueFeather);

describe('ButtonFab.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      type: 'activity',
      color: 'indigo',
    });
  });

  // Button
  it('renders correct button', () => {
    expect(component.$el.querySelector('button')).toBeTruthy();
  });

  it('renders correct color', () => {
    expect(
      component.$el.querySelector('button')._prevClass.indexOf('bg-indigo')
        !== -1,
    ).toBeTruthy();
  });

  // Events
  it('emits click event', () => {
    let test = null;
    const passed = true;

    component.$on('handleClick', (value) => {
      test = value;
    });

    component.onClick(true);

    expect(test).toEqual(passed);
  });
});
