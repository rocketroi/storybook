import Component from '@/components/button/Search';
import { getComponent } from '../../utils';

import Vue from 'vue';
import VueFeather from 'vue-feather';

Vue.use(VueFeather);

describe('ButtonSearch.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Button
  it('renders correct button', () => {
    expect(component.$el.querySelector('button')).toBeTruthy();
  });

  it('renders correct color', () => {
    const component = getComponent(Component, {
      color: 'indigo',
    });
    expect(
      component.$el.querySelector('button')._prevClass.indexOf('bg-indigo')
        !== -1,
    ).toBeTruthy();
  });

  it('loading renders correct spinner', () => {
    const component = getComponent(Component, {
      loading: true,
    });
    expect(component.$el.querySelector('.loading') !== -1).toBeTruthy();
  });

  it('sets disabled correctly', () => {
    const component = getComponent(Component, {
      disabled: true,
    });
    expect(component.$el.querySelector('button disabled') !== -1).toBeTruthy();
  });

  it('sets value correctly', () => {
    const component = getComponent(Component, {
      value: 'value',
    });
    expect(component.$el.querySelector('input').value).toEqual('value');
  });

  // Methods
  it('copy value correctly', () => {
    component.copyValue('value');
    expect(component._data.valueCopy).toEqual('value');
  });

  it('On blur is not active', () => {
    component.onBlur();
    expect(component.isActive).toEqual(false);
  });

  // Events
  it('emits click event', () => {
    let received = null;
    const expected = 'value';

    component.$on('handlerClick', (value) => {
      received = value;
    });
    component.onClick('value');

    expect(received).toEqual(expected);
  });

  it('emits on clear event', () => {
    let received = null;
    const expected = 'value';

    component.$on('handlerClear', (value) => {
      received = value;
    });
    component.onClear('value');
    expect(received).toEqual(expected);
  });
});
