import Component from '@/components/button/Link';
import { getComponent } from '../../utils';

describe('ButtonLink.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      label: 'label',
      color: 'indigo',
    });
  });

  // Button
  it('renders correct button', () => {
    expect(component.$el.querySelector('button')).toBeTruthy();
  });

  it('renders correct color', () => {
    const component = getComponent(Component, {
      label: 'label',
      color: 'indigo',
    });
    expect(
      component.$el
        .querySelector('button')
        ._prevClass.indexOf('text-indigo') !== -1,
    ).toBeTruthy();
  });

  // Events
  it('emits click event', () => {
    let test = null;
    const passed = true;

    component.$on('handleClick', (value) => {
      test = value;
    });

    component.onClick(true);

    expect(test).toEqual(passed);
  });
});
