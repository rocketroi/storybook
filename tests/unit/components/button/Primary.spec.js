import Component from '@/components/button/Primary';
import { getComponent } from '../../utils';

describe('ButtonPrimary.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      label: 'label',
      type: 'email',
    });
  });

  // Button
  it('renders correct button', () => {
    expect(component.$el.querySelector('button')).toBeTruthy();
  });

  it('renders correct color', () => {
    const component = getComponent(Component, {
      label: 'label',
      color: 'indigo',
    });
    expect(
      component.$el.querySelector('button')._prevClass.indexOf('bg-indigo')
        !== -1,
    ).toBeTruthy();
  });

  it('remove button shadow', () => {
    const component = getComponent(Component, {
      label: 'label',
      color: 'indigo',
      shadow: false,
    });
    expect(
      component.$el.querySelector('button')._prevClass.indexOf('shadow-indigo'),
    ).toBeTruthy();
  });

  it('renders button outlined', () => {
    const component = getComponent(Component, {
      label: 'label',
      color: 'indigo',
      shadow: false,
      outlined: true,
    });
    expect(
      component.$el
        .querySelector('button')
        ._prevClass.indexOf('is-outlined') !== -1,
    ).toBeTruthy();
  });

  // Events
  it('emits click event', () => {
    let test = null;
    const passed = true;

    component.$on('handleClick', (value) => {
      test = value;
    });

    component.onClick(true);

    expect(test).toEqual(passed);
  });
});
