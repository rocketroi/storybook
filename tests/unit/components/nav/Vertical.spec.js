import Component from '@/components/nav/Vertical';
import { getComponent } from '../../utils';

describe('Vertical.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.nav')).toBeTruthy();
  });
});
