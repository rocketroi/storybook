import Component from '@/components/card/Wrapper';
import { getComponent } from '../../utils';

describe('Wrapper.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.card')).toBeTruthy();
  });
});
