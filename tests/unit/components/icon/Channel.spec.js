import Component from '@/components/icon/Channel';
import { getComponent } from '../../utils';

describe('Channel.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component, {
      channel: 'ADW',
    });
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.channel')).toBeTruthy();
  });

  it('renders correct channel', () => {
    expect(component.$el.querySelector('.channel-ADW')).toBeTruthy();
  });

  it('change channel and renders correct channel', () => {
    component = getComponent(Component, {
      channel: 'FBK',
    });
    expect(component.$el.querySelector('.channel-FBK')).toBeTruthy();
  });
});
