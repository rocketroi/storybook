import Component from '@/components/tag/Tag';
import { getComponent } from '../../utils';

describe('Tag.vue', () => {
  let component;

  beforeEach(() => {
    component = getComponent(Component);
  });

  // Component
  it('renders correct component', () => {
    expect(component.$el.querySelector('.label')).toBeTruthy();
  });
});
