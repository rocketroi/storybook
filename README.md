# vue-storybook

A Vue project with Storybook library for the UI component development.

NPM script commands:

## Serve the storybook (on port 6006 if available):
```
npm run storybook
```

## Serve the storybook (on port 6006 if available):
```
npm run storybook
```

## Build the storybook:
```
npm run build-storybook
```

You need to add this project to your yarn packages to be used in any other project you want.

### Styles
To develop in SASS for an individual component just add the attribute `lang="sass"` to the style tag

For detailed explanation on how things work, checkout the [Vue guide](https://vuejs.org/) and [Storybook guide](https://storybook.js.org/basics/quick-start-guide/).
