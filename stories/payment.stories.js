import { storiesOf } from '@storybook/vue';

import {
  withKnobs, text, select,
} from '@storybook/addon-knobs';

import PaymentCard from '@/components/payment/Card';

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  White: 'white',
};

storiesOf('Payment', module)
  .addDecorator(withKnobs)
  .add(
    'Card',
    () => ({
      components: { PaymentCard },
      props: {
        brand: {
          default: text('Brand of card', 'VISA'),
        },
        name: {
          default: text('Card holder', 'Robin Boost'),
        },
        number: {
          default: text('Last 4 digits', '3251'),
        },
        expiryDate: {
          default: text('Expiry date', '04/19'),
        },
        color: {
          default: select('Has color', colors, 'indigo'),
        },
      },
      template: `
        <payment-card
          :brand="brand"
          :name="name"
          :number="number"
          :expiryDate="expiryDate"
          :color="color"
        />
      `,
      propsDescription: {
        PaymentCard: {
          brand: 'The name of card brand.',
          name: 'The name of card holder.',
          number: 'The last 4 digits.',
          expiryDate: 'Card expirate date (Ex. 04/19)',
          color: 'Applies the color card.',
        },
      },
    }),
    {
      info: {
        summary: `
            Preview of payment card
            ### How to _import_
            \`\`\`js
            import PaymentCard from 'vue-storybook/src/components/Payment/card';
            \`\`\`
          `,
      },
    },
  );
