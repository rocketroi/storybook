import Vue from 'vue';
import { storiesOf } from '@storybook/vue';

import {
  withKnobs, select,
} from '@storybook/addon-knobs';

import VueFeather from '../node_modules/vue-feather';

import IconChannel from '@/components/icon/Channel';

Vue.use(VueFeather);

const channels = {
  'Google Ads': 'ADW',
  'Google Shopping': 'GSH',
  'Bing Ads': 'BNG',
  'Amazon Ads': 'AMZ',
  'Facebook Ads': 'FBK',
};

const types = {
  Null: null,
  Search: 'SEARCH',
  Shopping: 'SHOPPING',
  Display: 'DISPLAY',
  Brand_Awareness: 'BRAND_AWARENESS',
  Reach: 'REACH',
  Traffic: 'LINK_CLICKS',
  Interaction: 'EVENT_RESPONSES',
  App_Installs: 'APP_INSTALLS',
  Video: 'VIDEO_VIEWS',
  Lead: 'LEAD_GENERATION',
  Messages: 'MESSAGES',
  Conversions: 'CONVERSIONS',
  Product_Catalog: 'PRODUCT_CATALOG_SALES',
  Local_Awareness: 'LOCAL_AWARENESS',
};

storiesOf('Icon', module)
  .addDecorator(withKnobs)
  .add(
    'Channel',
    () => ({
      components: { IconChannel },
      props: {
        channel: {
          default: select('Is channel', channels, 'ADW'),
        },

        type: {
          default: select('Is type', types, null),
        },
      },
      template: `
        <icon-channel
          :channel="channel"
          :type="type"
        />
      `,
      propsDescription: {
        IconChannel: {
          channel: 'Shows the icon of channel - [ADW, GSH, BNG, AMZ, FBK]',
          type: 'Shows the type of channel - [SEARCH, SHOPPING, DISPLAY, BRAND_AWARENESS, REACH, TRAFFIC, INTERACTION, APP_INSTALLS, VIDEO_VIEWS, LEAD, MESSAGES, CONVERSIONS, PRODUCT_CATALOG_SALES, LOCAL_AWARENESS]',
        },
      },
    }),
    {
      info: {
        summary: `
            Icon of channel and type.
            ### How to _import_
            \`\`\`js
            import IconChannel from 'vue-storybook/src/components/icon/Channel';
            \`\`\`
          `,
      },
    },
  );
