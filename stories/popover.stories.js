import { storiesOf } from '@storybook/vue';

import {
  withKnobs, select,
} from '@storybook/addon-knobs';

import Popover from '@/components/popover/Popover';
import CardWrapper from '@/components/card/Wrapper';
import ButtonFab from '@/components/button/Fab';

const directions = {
  Right: 'right',
  Left: 'left',
  Top: 'top',
};

storiesOf('Popover', module)
  .addDecorator(withKnobs)
  .add(
    'Popover',
    () => ({
      components: { Popover, CardWrapper, ButtonFab },
      props: {
        direction: {
          default: select('Has direction', directions, 'top'),
        },
      },
      template: `
        <popover
          :direction="direction"
        >
          <div slot="activator">
            <button-fab>
              <feather type="activity"/>
            </button-fab>
          </div>
          <div slot="container">
            <card-wrapper
              :shadow="true"
            >
              Content
              <br><br><br><br><br><br><br><br><br><br><br>
              Footer
            </card-wrapper>
          </div>
        </popover>
      `,
      propsDescription: {
        Popover: {
          direction: 'Show in direction defined - [left, right, top]',
        },
      },
    }),
    {
      info: {
        summary: `
          Popovers are small overlay content containers
          ### How to _import_
          \`\`\`js
          import Popover from 'vue-storybook/src/components/popover/Popover';
          \`\`\`
        `,
      },
    },
  );
