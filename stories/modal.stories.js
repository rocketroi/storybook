import { storiesOf } from '@storybook/vue';
import {
  withKnobs,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import Modal from '@/components/modal/Modal';
import ModalFullScreen from '@/components/modal/ModalFullScreen';
import ButtonLink from '@/components/button/Link';

storiesOf('Modal', module)
  .addDecorator(withKnobs)
  .add(
    'Modal',
    () => ({
      components: {
        Modal,
        ButtonLink,
      },
      methods: { onClick: action('Value') },
      template: `
        <div>
          <button @click="$refs.modal.visible = true">Show Modal</button>
          <modal
            ref="modal"
            :maxWidth="450"
            @handleClose="onClick"
          >
            <div slot="header">Title of modal</div>
            <div slot="body">
              <span class="font-14 text-grey">
                We will optimize where and when your ads are shown based on this limit.
              </span>
            </div>
            <div slot="footer">
              <div class="is-flex has-items-end">
                <button-link
                  color="grey"
                  @handleClick="$refs.modal.visible = false"
                >
                  Cancel
                </button-link>
                <span style="width: 24px" />
                <button-link
                  color="indigo"
                  @handleClick="onClick"
                >
                  Action
                </button-link>
              </div>
            </div>
          </modal>
        </div>
      `,
    }),
    {
      info: {
        summary: `
            Preview of modal component
            ### How to _import_
            \`\`\`js
            import Modal from 'vue-storybook/src/components/modal/Modal';
            \`\`\`
          `,
      },
    },
  );

storiesOf('Modal', module)
  .addDecorator(withKnobs)
  .add(
    'ModalFullScreen',
    () => ({
      components: {
        ModalFullScreen,
      },
      methods: { onClick: action('Value') },
      template: `
        <div>
          <button @click="$refs.modal.visible = true">Show Modal</button>
          <modal-full-screen
            ref="modal"
            @handleClose="onClick"
          >
            <div class="text-center mt-64">
              <p class="heading mb-25">Title of modal</p>
              <span class="font-14 text-grey">
                We will optimize where and when your ads are shown based on this limit.
              </span>
            </div>
          </modal-full-screen>
        </div>
      `,
    }),
    {
      info: {
        summary: `
            Preview of modal full screen component
            ### How to _import_
            \`\`\`js
            import ModalFullScreen from 'vue-storybook/src/components/modal/ModalFullScreen';
            \`\`\`
          `,
      },
    },
  );
