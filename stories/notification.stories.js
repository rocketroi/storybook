import { storiesOf } from '@storybook/vue';

import {
  withKnobs,
  text,
  select,
  boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import NotificationAdvise from '@/components/notification/Advise';
import NotificationToasted from '@/components/notification/Toasted';
import NotificationAlert from '@/components/notification/Alert';
import ButtonLink from '@/components/button/Link';

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  White: 'white',
};

storiesOf('Notification', module)
  .addDecorator(withKnobs)
  .add(
    'Advise',
    () => ({
      components: { NotificationAdvise },
      props: {
        visible: {
          default: boolean('Is visible', true),
        },
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        close: {
          default: boolean('Has close button', true),
        },
      },
      methods: { onClose: action('Value') },
      template: `
        <notification-advise
          :visible="visible"
          :color="color"
          :close="close"
          @handleClose="onClose"
        >
          <div slot="icon"><feather type="alert-circle"/></div>
          <div slot="label">You have selected 30 products. <a href="#">Edit Google Merchant category</a></div>
        </notification-advise>
      `,
      propsDescription: {
        NotificationAdvise: {
          visible: 'Option used to trigger the notification visibility.',
          color: 'Applies the background color',
          close: 'Add or remove the close button',
        },
      },
    }),
    {
      info: {
        summary: `
            Notifications are used to show alert or information to users.
            ### How to _import_
            \`\`\`js
            import NotificationAdvise from 'vue-storybook/src/components/notification/Advise';
            \`\`\`
          `,
      },
    },
  )
  .add(
    'Toasted',
    () => ({
      components: { NotificationToasted },
      template: `
        <notification-toasted />
      `,
    }),
    {
      info: {
        summary: `
            Toasted notifications are used to show success, error or information to users.
            See more documentation on https://github.com/shakee93/vue-toasted
          `,
      },
    },
  )
  .add(
    'Alert',
    () => ({
      components: {
        NotificationAlert,
        ButtonLink,
      },
      props: {
        icon: {
          default: text('Icon', 'alert-circle'),
        },
        title: {
          default: text('Title', 'Shopping unlocked'),
        },
        subtitle: {
          default: text('Subtitle', 'You can now start creating Shooping Ads. Come on, there`s a lot of stuff to sell!'),
        },
      },
      methods: { onClick: action('Value') },
      template: `
        <div>
          <button @click="$refs.notificationAlert.visible = true">Show Alert</button>
          <notification-alert
            ref="notificationAlert"
            :icon="icon"
            :title="title"
            :subtitle="subtitle"
            @handleClose="onClick"
          >
            <div slot="footer">
              <div class="is-flex has-items-end">
                <button-link
                  color="indigo"
                  @handleClick="$refs.notificationAlert.visible = false"
                >
                  Great
                </button-link>
              </div>
            </div>
          </notification-alert>
        </div>
      `,
    }),
    {
      info: {
        summary: `
          Notifications alert are used to show alert or information to users.
          ### How to _import_
          \`\`\`js
          import NotificationAlert from 'vue-storybook/src/components/notification/Alert';
          \`\`\`
          `,
      },
    },
  );
