import { storiesOf } from '@storybook/vue';

import {
  withKnobs,
  text,
  select,
  number,
  boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import FormInput from '@/components/form/Input';
import FormTextarea from '@/components/form/Textarea';
import FormSelect from '@/components/form/Select';
import FormSwitch from '@/components/form/Switch';
import FormCheckbox from '@/components/form/Checkbox';
import FormChip from '@/components/form/Chip';

const type = {
  Email: 'email',
  Url: 'url',
  Number: 'number',
  Password: 'password',
  Text: 'text',
  Tel: 'tel',
};

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  White: 'white',
};

const options = {
  checked: 'On',
  unchecked: 'Off',
};

storiesOf('Form (Work in progress)', module)
  .addDecorator(withKnobs)
  .add(
    'Input',
    () => ({
      components: { FormInput },
      props: {
        value: {
          default: text('Value', null),
        },
        type: {
          default: select('Type of input', type, 'text'),
        },
        label: {
          default: text('Has label', 'Your name'),
        },
        placeholder: {
          default: text('Has placeholder', undefined),
        },
        readonly: {
          default: boolean('Is readonly', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
        minLength: {
          default: number('Has min length', undefined),
        },
        maxLength: {
          default: number('Has max length', undefined),
        },
        required: {
          default: boolean('Is required', false),
        },
        autocomplete: {
          default: text('Is autocomplete', undefined),
        },
        helpMessage: {
          default: text('Has help Message', 'Helper text'),
        },
        errorMessages: {
          default: text('Has error Messages', undefined),
        },
      },
      methods: { onInput: action('Value') },
      template: `
        <form-input
          :value="value"
          :type="type"
          :label="label"
          :placeholder="placeholder"
          :readonly="readonly"
          :disabled="disabled"
          :min-length="minLength"
          :max-length="maxLength"
          :required="required"
          :help-message="helpMessage"
          :error-messages="errorMessages"
          @handleInput="onInput"
        />
      `,
      propsDescription: {
        FormInput: {
          value: 'Input value',
          type: 'The input type. Similar to HTML5 type attribute.',
          label: 'Sets input label',
          placeholder:
            'The placeholder attribute specifies a short hint that describes the expected value.',
          readonly: 'Puts input in readonly state.',
          disabled: 'Disable the input.',
          minLength: 'set a minlength.',
          maxLength: 'set a maxlength.',
          required: 'The input required. Similar to HTML5 required attribute.',
          helpMesssage: 'Hint text',
          errorMessages:
            'Puts the input in an error state and passes through custom error messsages.',
        },
      },
    }),
    {
      info: {
        summary: `
        Text fields allow users to input, edit and select text typically inside forms. But they can appear in other places as well like dialog boxes and search.
          ### How to _import_
          \`\`\`js
          import FormInput from 'vue-storybook/src/components/form/Input';
          \`\`\`
        `,
      },
    },
  )
  .add(
    'Textarea',
    () => ({
      components: { FormTextarea },
      props: {
        value: {
          default: text('Value', null),
        },
        label: {
          default: text('Has label', 'Write a description'),
        },
        placeholder: {
          default: text('Has placeholder', 'e.g. Hello world'),
        },
        required: {
          default: boolean('Is required', false),
        },
        readonly: {
          default: boolean('Is readonly', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
        minLength: {
          default: number('Has min length', null),
        },
        maxLength: {
          default: number('Has max length', null),
        },
        rows: {
          default: number('Number of rows', 8),
        },
        helpMessage: {
          default: text('Has help Message', null),
        },
        errorMessages: {
          default: text('Has error Messages', null),
        },
      },
      methods: { onInput: action('Value') },
      template: `
        <form-textarea
          :value="value"
          :label="label"
          :placeholder="placeholder"
          :required="required"
          :readonly="readonly"
          :disabled="disabled"
          :min-length="minLength"
          :max-length="maxLenght"
          :rows="rows"
          :help-message="helpMessage"
          :error-messages="errorMessages"
          @handleInput="onInput"
        />
      `,
      propsDescription: {
        FormTextarea: {
          value: 'Textarea value',
          label: 'Sets textarea label',
          placeholder:
            'The placeholder attribute specifies a short hint that describes the expected value.',
          required:
            'When present, it specifies that a textarea is required/must be filled out',
          readonly: 'Puts textarea in readonly state.',
          disabled: 'Disable the textarea',
          minLength:
            'The minlength attribute specifies the minimum length (in characters) of a textarea.',
          maxLength:
            'The maxlength attribute specifies the maximum length (in characters) of a textarea.',
          rows: 'The visible height of a textarea, in lines.',
          helpMessage:
            'The help message is shown to describe better what to introduce',
          errorMessages:
            'The error message is shown when the value value is not valid',
        },
      },
    }),
    {
      info: {
        summary: `
        Text fields allow users to input, edit and select text typically inside forms. But they can appear in other places as well like dialog boxes and search.
          ### How to _import_
          \`\`\`js
          import FormTextarea from 'vue-storybook/src/components/form/Textarea';
          \`\`\`
          `,
      },
    },
  )
  .add(
    'Switch',
    () => ({
      components: { FormSwitch },
      props: {
        labels: {
          default: options,
        },
        value: {
          default: boolean('Initial state', false),
        },
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
      },
      methods: { onChange: action('Value') },
      template: `
        <form-switch
          :labels="labels"
          :value="value"
          :color="color"
          :disabled="disabled"
          @handleChange="onChange"
        />
      `,
      propsDescription: {
        FormSwitch: {
          labels:
            'If Boolean - shows/hides default labels ("on" and "off") If Object - sets custom labels for both states. Example: {checked: "Foo", unchecked: "Bar"}',
          value: 'The value of the switch.',
          color:
            'Applies specified color - ["indigo", "green", "yellow", "red", "white"]',
          disabled: 'Button does not react on mouse events.',
        },
      },
    }),
    {
      info: {
        summary: `
        On/off switches toggle the state of a single settings option. The option that the switch controls, as well as the state it’s in, should be made clear from the corresponding inline label.
          ### How to _import_
          \`\`\`js
          import FormSwitch from 'vue-storybook/src/components/form/Switch';
          \`\`\`
          `,
      },
    },
  );

storiesOf('Form (Work in progress)', module)
  .addDecorator(withKnobs)
  .add(
    'Select',
    () => ({
      components: { FormSelect },
      props: {
        value: {
          default: null,
        },
        label: {
          default: text('Has label', 'Select an option'),
        },
        options: {
          default: [
            { label: 'Google Ads', code: 'GA' },
            { label: 'Google Shopping', code: 'GS' },
            { label: 'Amazon Ads', code: 'AA' },
            { label: 'Facebook Ads', code: 'FA' },
          ],
        },
        searchable: {
          default: boolean('Is searchable', false),
        },
        multiple: {
          default: boolean('Is multiple selection', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
        errorMessages: {
          default: text('Has message error', ''),
        },
      },
      methods: { onInput: action('Value') },
      template: `
        <form-select
          :value="value"
          :label="label"
          :options="options"
          :searchable="searchable"
          :multiple="multiple"
          :disabled="disabled"
          :error-messages="errorMessages"
          @handleInput="onInput"
        />
      `,
      propsDescription: {
        FormSelect: {
          value: 'Contains the currently selected value.',
          label: 'Sets select label',
          options:
            "An array of strings or objects to be used as dropdown choices. If you are using an array of objects, vue-select will look for a label key (ex. [{label: 'Canada', value: 'CA'}]). A custom label key can be set with the label prop.",
          searchable: 'Enable/disable filtering the options.',
          multiple: 'Equivalent to the multiple attribute on a <select> input.',
          disabled: 'Disable the entire component.',
          errorMessages:
            'The error message is shown when the value value is not valid',
        },
      },
    }),
    {
      info: {
        summary: `
        A select picks between multiple options. The select displays the current state and a down arrow. They can have single selection or multiple.
        ### How to _import_
        \`\`\`js
        import FormSelect from 'vue-storybook/src/components/form/Select';
        \`\`\`
        `,
      },
    },
  );

storiesOf('Form (Work in progress)', module)
  .addDecorator(withKnobs)
  .add(
    'Checkbox',
    () => ({
      components: { FormCheckbox },
      props: {
        id: {
          default: text('Id', 'mycheck1'),
        },
        checked: {
          default: boolean('Checked', false),
        },
        value: {
          default: text('Value', 'cat'),
        },
        disabled: {
          default: boolean('Disabled', false),
        },
        color: {
          default: select('Color', colors, 'indigo'),
        },
      },
      data: () => ({
        check: undefined,
      }),
      methods: { onChange: action('Value') },
      template: `
        <form-checkbox
          :id="id"
          v-model="check"
          :value="value"
          :checked="checked"
          :color="color"
          :disabled="disabled"
        >
          Checkbox
        </form-checkbox>
      `,
      propsDescription: {
        FormCheckbox: {
          id: 'Recommended. input id associated with label',
          model:
            'The model variable to bind the selection value. If it is an array, it will toggle values inside of it.',
          checked: 'is checked by default?',
          value: 'Value for input, without it checkbox works as true/false',
          disabled: 'HTML required attr',
          color:
            'Pass the color of checkbox - ["indigo", "green", "yellow", "red", "white"]',
        },
      },
    }),
    {
      info: {
        summary: `
          Checkboxes allow the user to select multiple options from a set.
          ### How to _import_
          \`\`\`js
          import FormCheckbox from 'vue-storybook/src/components/form/Checkbox';
          \`\`\`
          `,
      },
    },
  )
  .add(
    'Chip',
    () => ({
      components: { FormChip },
      props: {
        color: {
          default: select('Has color', colors, 'light-grey-1'),
        },

        shadow: {
          default: boolean('Has shadow', false),
        },

        close: {
          default: boolean('Has close button', false),
        },
      },
      methods: {
        onClick: action('Click'),
        onClose: action('Close'),
      },
      template: `
        <form-chip
          :color="color"
          :close="close"
          :shadow="shadow"
          @handleClick="onClick"
          @handleClose="onClose"
        >
          <div slot="icon"><feather type="activity"/></div>
          <div slot="label">I'm a chip</div>
        </form-chip>
      `,
      propsDescription: {
        FormChip: {
          color: 'Applies a specific background color.',
          close: 'Adds button to remove chip.',
          shadow: 'Applies a shadow of the same color specified.',
        },
      },
    }),
    {
      info: {
        summary: `
            Chips represent complex entities in small blocks, such as a contact. They can be used as a way for a user to create arbitrary items, like categories or tags.
            ### How to _import_
            \`\`\`js
            import FormChip from 'vue-storybook/src/components/form/Chip';
            \`\`\`
          `,
      },
    },
  );
