import { storiesOf } from '@storybook/vue';

import {
  withKnobs,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import TabsHoritzontal from '@/components/tabs/Horitzontal';

storiesOf('Tabs', module)
  .addDecorator(withKnobs)
  .add(
    'Horitzontal',
    () => ({
      components: { TabsHoritzontal },
      props: {
        options: {
          default: [
            {
              value: 'GA',
              label: 'Google Ads',
              active: false,
              badge: 2,
            },
            {
              value: 'GS',
              label: 'Google Shopping',
              active: true,
              badge: 0,
            },
            {
              value: 'B',
              label: 'Bing',
              active: false,
              badge: null,
            },
            {
              value: 'A',
              label: 'Amazon',
              active: false,
            },
          ],
        },
      },
      methods: { onClick: action('Value') },
      template: `
        <tabs-horitzontal
          :options="options"
          @handleClick="onClick"
        />
      `,
      propsDescription: {
        TabsHoritzontal: {
          options:
            "An array of objects to be used as option choices. (ex. [{value: 'GA', label: 'Google Ads', active: false}]).",
        },
      },
    }),
    {
      info: {
        summary: `
            Horizontal navigation
            ### How to _import_
            \`\`\`js
            import TabsHoritzontal from 'vue-storybook/src/components/tabs/Horitzontal';
            \`\`\`
          `,
      },
    },
  );
