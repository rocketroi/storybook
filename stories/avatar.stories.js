import { storiesOf } from '@storybook/vue';

import {
  withKnobs, text, select, boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import AvatarUser from '@/components/avatar/User';
import AvatarCompany from '@/components/avatar/Company';

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  White: 'white',
};

const sizesUser = {
  XL: 'xl',
  LG: 'lg',
  SM: 'sm',
  XS: 'xs',
};

const sizesCompany = {
  Large: 'lg',
  Medium: 'md',
  Small: 'sm',
};

storiesOf('Avatar', module)
  .addDecorator(withKnobs)
  .add(
    'User',
    () => ({
      components: { AvatarUser },
      props: {
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        size: {
          default: select('Size', sizesUser, 'xl'),
        },
        path: {
          default: text(
            'Path photo',
            'https://picturepan2.github.io/spectre/img/avatar-1.png',
          ),
        },
        name: {
          default: text('Name of user', 'Peter'),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <avatar-user
          :color="color"
          :size="size"
          :path="path"
          :name="name"
          @handleClick="onClick"
        />
      `,
      propsDescription: {
        AvatarUser: {
          color: 'Applies the background color',
          size:
            'Applies differents sizes to the avatar - ["xl", "lg", "sm", "xs"] - [64px, 48px, 24px, 32px]',
          photo: 'Path of image profile',
          name:
            'The name appear inside the avatar if image is broken or not exist.',
        },
      },
    }),
    {
      info: {
        summary: `
            Avatars can be used to represent people. When used with a specific logo, avatars can also be used to represent a brand. They also can be a placeholder when there is no image to be shown, showing a initials letters of a name on contacts that have no picture yet.
            ### How to _import_
            \`\`\`js
            import AvatarUser from 'vue-storybook/src/components/avatar/User';
            \`\`\`
          `,
      },
    },
  );

storiesOf('Avatar', module)
  .addDecorator(withKnobs)
  .add(
    'Company',
    () => ({
      components: { AvatarCompany },
      props: {
        color: {
          default: select('Color', colors, 'indigo'),
        },
        size: {
          default: select('Size', sizesCompany, 'lg'),
        },
        name: {
          default: text('Name', 'Spaceboost'),
        },
        inactive: {
          default: boolean('Has inactive state', false),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <avatar-company
          :color="color"
          :size="size"
          :name="name"
          :inactive="inactive"
          @handleClick="onClick"
        />
      `,
      propsDescription: {
        AvatarCompany: {
          color:
            'Applies specified background color - ["indigo", "green", "yellow", "red", "white"]',
          size:
            'Applies differents sizes to the avatar - ["lg", "md", "sm"] - [204x204, 32x32, 24x24]',
          name: 'The initials attribute is the name appear inside the avatar.',
          inactive: 'Add inactive state.',
        },
      },
    }),
    {
      info: {
        summary: `
            Avatars are user profile pictures.
            ### How to _import_
            \`\`\`js
            import AvatarCompany from 'vue-storybook/src/components/avatar/Company';
            \`\`\`
          `,
      },
    },
  );
