import Vue from 'vue';
import { storiesOf } from '@storybook/vue';
import {
  withKnobs, text, select, boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import VueFeather from '../node_modules/vue-feather';

import ButtonPrimary from '@/components/button/Primary';
import ButtonFab from '@/components/button/Fab';
import ButtonSearch from '@/components/button/Search';
import ButtonLink from '@/components/button/Link';
import ButtonItem from '@/components/button/Item';

Vue.use(VueFeather);

const colors = {
  NULL: null,
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  Light: 'light-grey-1',
  White: 'white',
};

storiesOf('Button', module)
  .addDecorator(withKnobs)
  .add(
    'Primary',
    () => ({
      components: { ButtonPrimary },
      props: {
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        loading: {
          default: boolean('Is loading', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
        outlined: {
          default: boolean('Is outlined', false),
        },
        shadow: {
          default: boolean('Has shadow', true),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <button-primary
          :color="color"
          :loading="loading"
          :disabled="disabled"
          :outlined="outlined"
          :shadow="shadow"
          @handleClick="onClick"
        >
          <div slot="icon"><feather type="layers"/></div>
          <div slot="label">I'm a button</div>
        </button-primary>
      `,

      propsDescription: {
        ButtonPrimary: {
          color:
            'Applies specified color to the button - ["indigo", "green", "yellow", "red", "white"].',
          loading: 'Adds a loading icon animation.',
          disabled: 'Disables the button and prevents its actions.',
          outlined: 'Change design of button to outlined version.',
          shadow: 'Add or remove shadow color.',
        },
      },
    }),
    {
      info: {
        summary: `
            The classic button, in different colors, sizes, and states.
            ### How to _import_
            \`\`\`js
            import ButtonPrimary from 'vue-storybook/src/components/button/Primary';
            \`\`\`
          `,
      },
    },
  )
  .add(
    'Fab',
    () => ({
      components: { ButtonFab },
      props: {
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        textColor: {
          default: select('Has text color', colors, null),
        },
        loading: {
          default: boolean('Is loading', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
        shadow: {
          default: boolean('Has shadow', true),
        },
        outlined: {
          default: boolean('Is outlined', false),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <button-fab
          :color="color"
          :text-color="textColor"
          :loading="loading"
          :disabled="disabled"
          :outlined="outlined"
          :shadow="shadow"
          @handleClick="onClick"
        >
          <feather type="activity"/>
        </button-fab>
      `,
      propsDescription: {
        ButtonFab: {
          color:
            'Applies specified color to the fab button - ["indigo", "green", "yellow", "red", "white"].',
          textColor:
            'Applies specified text color to the fab button - ["indigo", "green", "yellow", "red", "white"].',
          loading: 'Add the loading state.',
          disabled: 'Disables the button and prevents its actions.',
          outlined: 'Change design of button to outlined version.',
          shadow: 'Add or remove shadow color.',
        },
      },
    }),
    {
      info: {
        summary: `
            The classic fab button, in different colors, and states.
            ### How to _import_
            \`\`\`js
            import ButtonFab from 'vue-storybook/src/components/button/Fab';
            \`\`\`
          `,
      },
    },
  )
  .add(
    'Search',
    () => ({
      components: { ButtonSearch },
      props: {
        value: {
          default: text('Has value', ''),
        },
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        loading: {
          default: boolean('Is loading', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
      },
      methods: {
        onClear: action('handlerClear'),
        onClick: action('handlerClick'),
        onValue: action('handlerValue'),
      },
      template: `
        <button-search
          :value="value"
          :color="color"
          :loading="loading"
          :disabled="disabled"
          @handlerClick="onClick"
          @handlerValue="onValue"
          @handlerClear="onClear"
        />
      `,
      propsDescription: {
        ButtonSearch: {
          color:
            'Applies specified color to the button - ["indigo", "green", "yellow", "red", "white"].',
          loading: 'Add the loading state.',
          disabled: 'Disables the button and prevents its actions.',
        },
      },
    }),
    {
      info: {
        summary: `
            The classic search button, in different colors, and states.
            ### How to _import_
            \`\`\`js
            import ButtonSearch from 'vue-storybook/src/components/button/Search';
            \`\`\`
          `,
      },
    },
  )
  .add(
    'Link',
    () => ({
      components: { ButtonLink },
      props: {
        color: {
          default: select('Has color', colors, 'indigo'),
        },
        loading: {
          default: boolean('Is loading', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <button-link
          :color="color"
          :loading="loading"
          :disabled="disabled"
          @handleClick="onClick"
        >
          I'm a button link
        </button-link>
      `,
      propsDescription: {
        ButtonLink: {
          color:
            'Applies specified color to the button - ["indigo", "green", "yellow", "red", "white"].',
          loading: 'Add the loading state.',
          disabled: 'Disables the button and prevents its actions.',
        },
      },
    }),
    {
      info: {
        summary: `
            The minimal link button, in different colors, and states.
            ### How to _import_
            \`\`\`js
            import ButtonLink from 'vue-storybook/src/components/button/Link';
            \`\`\`
          `,
      },
    },
  )
  .add(
    'Item',
    () => ({
      components: { ButtonItem },
      props: {
        active: {
          default: boolean('Is active', false),
        },
        disabled: {
          default: boolean('Is disabled', false),
        },
      },
      methods: { onClick: action('Click') },
      template: `
        <button-item
          :active="active"
          :disabled="disabled"
          @handleClick="onClick"
        >
          I'm a item
        </button-item>
      `,
      propsDescription: {
        ButtonItem: {
          active: '',
          disabled: 'Disables the button and prevents its actions.',
        },
      },
    }),
    {
      info: {
        summary: `
            The minimal item button, in different states.
            ### How to _import_
            \`\`\`js
            import ButtonItem from 'vue-storybook/src/components/button/Item';
            \`\`\`
          `,
      },
    },
  );
