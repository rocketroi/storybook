import Vuex from 'vuex';
import Vue from 'vue';

import { storiesOf } from '@storybook/vue';
import Colors from '@/components/general/Colors';
import Typography from '@/components/general/Typography';
import Icons from '@/components/general/Icons';
import colors from '@/assets/data/colors.json';

Vue.use(Vuex);

function FullWidth() {
  return {
    template: `
      <div style="
        position: absolute;
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
      ">
        <story/>
      </div>`,
  };
}

storiesOf('Spaceboost', module)
  .addDecorator(FullWidth)
  .add(
    'Colors',
    () => ({
      components: { Colors },
      template: '<colors/>',
      store: new Vuex.Store({
        state: { colors },
      }),
    }),
    {
      info: {
        source: false,
        components: { Colors },
        summary: `
          Color utilities are used for changing colors for text, link and background.
          ### How to _usage_
          \`\`\`js
          <!-- Text colors -->
          <span class="text-{color}">text color</span>
          \`\`\`
          \`\`\`js
          <!-- Text invert colors -->
          <span class="text-{color}-invert">text invert color</span>
          \`\`\`
          \`\`\`js
          <!-- Background colors -->
          <span class="bg-{color}">background color</span>
          \`\`\`
          \`\`\`js
          <!-- Shadow colors -->
          <span class="shadow-{color}">shadow box color</span>
          \`\`\`
        `,
      },
    },
  )

  .add(
    'Typography',
    () => ({
      components: { Typography },
      template: '<typography/>',
    }),
    {
      info: {
        source: false,
        summary: `
          Typography sets default styles for headings, paragraphs, semantic text, blockquote and lists elements.
          ### How to _usage_
          \`\`\`js
          <!-- Headings -->
          <h1>H1 Heading</h1>
          <span class="h1">H1 Heading</span>
          \`\`\`
          \`\`\`js
          <!-- Paragraphs -->
          <p>Lorem ipsum dolor sit amet, ...</p>
          <p>...</p>
          \`\`\`
          ### Text utilites
          \`\`\`js
          <!-- left-aligned text -->
          <div class="text-left"></div>
          <!-- center-aligned text -->
          <div class="text-center"></div>
          <!-- right-aligned text -->
          <div class="text-right"></div>
          <!-- justified text -->
          <div class="text-justify"></div>

          <!-- Lowercased text -->
          <div class="text-lowercase"></div>
          <!-- Uppercased text -->
          <div class="text-uppercase"></div>
          <!-- Capitalized text -->
          <div class="text-capitalize"></div>

          <!-- Normal weight text -->
          <div class="text-normal"></div>
          <!-- Bold text -->
          <div class="text-bold"></div>
          <!-- Italicized text -->
          <div class="text-italic"></div>
          <!-- Larger text (120%) -->
          <div class="text-large"></div>

          <!-- Overflow behavior: display an ellipsis to represent clipped text -->
          <div class="text-ellipsis"></div>
          <!-- Overflow behavior: truncate the text -->
          <div class="text-clip"></div>
          <!-- Text may be broken at arbitrary points -->
          <div class="text-break"></div>
          \`\`\`
        `,
      },
    },
  )

  .add(
    'Icons',
    () => ({
      components: { Icons },
      template: '<icons />',
    }),
    {
      info: {
        source: false,
        summary: `
          Feather is a collection of simply beautiful open source icons.
          ### How to _usage_
          \`\`\`js
          <feather type="name"></feather>
          \`\`\`
        `,
      },
    },
  );
