import { storiesOf } from '@storybook/vue';

import {
  withKnobs, select, boolean, text,
} from '@storybook/addon-knobs';

import CardWrapper from '@/components/card/Wrapper';
import CardItem from '@/components/card/Item';
import CardSection from '@/components/card/Section';

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  Light: 'light-grey-1',
  White: 'white',
};

storiesOf('Card', module)
  .addDecorator(withKnobs)
  .add(
    'Wrapper',
    () => ({
      components: { CardWrapper },
      props: {
        color: {
          default: select('Has color', colors, 'white'),
        },

        padding: {
          default: boolean('Has padding', true),
        },

        shadow: {
          default: boolean('Has shadow', true),
        },
      },
      template: `
        <card-wrapper
          :color="color"
          :padding="padding"
          :shadow="shadow"
          overflow="hidden"
        >
          <div slot="title">Main title</div>
          <div slot="subtitle">Subtitle of content</div>
          <div class="empty">
            <div class="empty-icon">
              <feather type="box"/>
            </div>
            <p class="empty-title h5">Light and minimal wrapper</p>
            <p class="empty-subtitle">Add text, images, forms or any content.</p>
          </div>
        </card-wrapper>
      `,
      propsDescription: {
        CardWrapper: {
          color:
            'Applies specified color of background - ["indigo", "green", "yellow", "red", "white"].',
          padding: 'Adds 40px of padding.',
          shadow: 'Adds border shadow',
          overflow:
            'Adds type of overflow - ["scroll", "hidden", "auto", "visible"].',
        },
      },
    }),
    {
      info: {
        summary: `
            Cards may contain a photo, text, and a link about a single subject. They may display content containing elements of varying size, such as photos with captions of variable length.
            ### How to _import_
            \`\`\`js
            import CardWrapper from 'vue-storybook/src/components/card/Wrapper';
            \`\`\`
          `,
      },
    },
  );

storiesOf('Card', module)
  .addDecorator(withKnobs)
  .add(
    'Item',
    () => ({
      components: { CardItem },
      props: {
        icon: {
          default: text('Has icon', 'layers'),
        },

        title: {
          default: text('Has title', 'Title item'),
        },

        subtitle: {
          default: text('Has subtitle', '45 items'),
        },

        isEmpty: {
          default: boolean('Is empty', false),
        },
      },
      template: `
        <card-item
          :icon="icon"
          :title="title"
          :subtitle="subtitle"
          :is-empty="isEmpty"
        />
      `,
      propsDescription: {
        CardWrapper: {
          icon: 'Name feather icon',
          title: 'String',
          subtitle: 'String',
          isEmpty: 'Boolean',
        },
      },
    }),
    {
      info: {
        summary: `
            Simple card component
            ### How to _import_
            \`\`\`js
            import CardItem from 'vue-storybook/src/components/card/Item';
            \`\`\`
          `,
      },
    },
  );

storiesOf('Card', module)
  .addDecorator(withKnobs)
  .add(
    'Section',
    () => ({
      components: { CardSection },
      props: {
        icon: {
          default: text('Has icon', 'layers'),
        },

        title: {
          default: text('Has title', 'Title item'),
        },

        subtitle: {
          default: text('Has subtitle', '45 items'),
        },
      },
      template: `
        <card-section
          :icon="icon"
          :title="title"
          :subtitle="subtitle"
        />
      `,
      propsDescription: {
        CardWrapper: {
          icon: 'Name feather icon',
          title: 'String',
          subtitle: 'String',
        },
      },
    }),
    {
      info: {
        summary: `
            Section card component
            ### How to _import_
            \`\`\`js
            import CardSection from 'vue-storybook/src/components/card/Section';
            \`\`\`
          `,
      },
    },
  );
