import { storiesOf } from '@storybook/vue';

import {
  withKnobs, select,
} from '@storybook/addon-knobs';

import Tag from '@/components/tag/Tag';

const colors = {
  Indigo: 'indigo',
  Green: 'green',
  Yellow: 'yellow',
  Red: 'red',
  Grey: 'grey',
  White: 'white',
};

storiesOf('Tag', module)
  .addDecorator(withKnobs)
  .add(
    'Tag',
    () => ({
      components: { Tag },
      props: {
        color: {
          default: select('Has color', colors, 'indigo'),
        },
      },
      template: `
        <tag
          :color="color"
        >
          Label
        </tag>
      `,
      propsDescription: {
        Tag: {
          color: 'Applies the background color',
        },
      },
    }),
    {
      info: {
        summary: `
            Small tag labels to insert anywhere
            ### How to _import_
            \`\`\`js
            import Tag from 'vue-storybook/src/components/tag/Tag';
            \`\`\`
          `,
      },
    },
  );
