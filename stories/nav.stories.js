import { storiesOf } from '@storybook/vue';

import {
  withKnobs,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import NavVertical from '@/components/nav/Vertical';

storiesOf('Nav', module)
  .addDecorator(withKnobs)
  .add(
    'Vertical',
    () => ({
      components: { NavVertical },
      props: {
        options: {
          default: [
            {
              value: 'GA',
              label: 'Google Ads',
              active: false,
            },
            {
              value: 'GS',
              label: 'Google Shopping',
              active: true,
            },
            {
              value: 'B',
              label: 'Bing',
              active: false,
            },
            {
              value: 'A',
              label: 'Amazon',
              active: false,
            },
          ],
        },
      },
      methods: { onClick: action('Value') },
      template: `
        <nav-vertical
          :options="options"
          @handleClick="onClick"
        />
      `,
      propsDescription: {
        NavVertical: {
          options:
            "An array of objects to be used as option choices. (ex. [{value: 'GA', label: 'Google Ads', active: false}]).",
        },
      },
    }),
    {
      info: {
        summary: `
            Nav vertical list of links or buttons for actions and navigation.
            ### How to _import_
            \`\`\`js
            import NavVertical from 'vue-storybook/src/components/nav/Vertical';
            \`\`\`
          `,
      },
    },
  );
